package com.example.proxymode;

import android.util.Log;

public class ProxyRoommate implements ISubject {

    private RealSubject realSubject;

    ProxyRoommate(RealSubject realSubject){
        this.realSubject = realSubject;
    }

    @Override
    public void buyKole() {
        Log.d("proxy","prepare money!");
        this.realSubject.buyKole();
        Log.d("proxy","go back!");
    }
}
