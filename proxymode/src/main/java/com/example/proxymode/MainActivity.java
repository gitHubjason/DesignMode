package com.example.proxymode;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.proxymode.R;

/**
 * 代理模式Proxy与Delegate
 * Proxy是被代理和代理实现了共同接口，然后代理关联（拥有）被代理实例，来实现功能
 * Delegate，仅为帮被委派方实现功能
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ProxyRoommate roommate = new ProxyRoommate(new RealSubject());
        roommate.buyKole();
    }
}
