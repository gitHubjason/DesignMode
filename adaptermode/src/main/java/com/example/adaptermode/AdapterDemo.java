package com.example.adaptermode;

/*
    适配器设计模式
    例子：适配器电源
    适配器两种模式：类适配器、对象适配器
 */

public class AdapterDemo {
    public static void test() {
        // 类适配
        ChinaPower chinaPower = new CPower();
        chinaPower.twoStep();

        // 对象适配
        ChinaPower chinaPower2 = new BPower();
        chinaPower2.twoStep();
    }
}
