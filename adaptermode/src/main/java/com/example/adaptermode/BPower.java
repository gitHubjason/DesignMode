package com.example.adaptermode;

/**
 * 对象适配
 */
public class BPower implements ChinaPower {
    //创建美国电源的对象，通过对象调用
    AmericaPower americaPower = new APower();

    public void twoStep() {
        americaPower.threeStep();
    }
}
