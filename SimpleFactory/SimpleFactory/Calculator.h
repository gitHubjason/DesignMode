/*
	面试题1：写一个计算器
*/

#include<iostream>
#include<string>
using namespace std;

#pragma once
class Calculator
{
public:
	Calculator();
	~Calculator();

	static void runCalculator() {
		string strNumberA;
		string strNumberB;
		string strOperator;
		string strResult;

		cout << "请输入数字A：" << endl;
		cin >> strNumberA;
		cout << "请输入运算符号(+-*/)：" << endl;
		cin >> strOperator;
		cout << "请输入数字B：" << endl;
		cin >> strNumberB;


		if (strOperator == "+") {
			strResult = to_string(stof(strNumberA) + stof(strNumberB));
		}
		else if (strOperator == "-") {
			strResult = to_string(stof(strNumberA) - stof(strNumberB));
		}
		else if (strOperator == "*") {
			strResult = to_string(stof(strNumberA) * stof(strNumberB));
		}
		else if (strOperator == "/") {
			if (strNumberB != "0") {
				strResult = to_string(stof(strNumberA) / stof(strNumberB));
			}
			else {
				strResult = "除数不能为0！";
			}
			
		}
		
		cout << strResult << endl;
	}
};

