#pragma once
class Operation
{
public:
	Operation();
	~Operation();

	double setNumberA();
	double setNumberB(); 
	virtual double getResult();

private:
	double _numberA = 0.0;
	double _numberB = 0.0;

};

